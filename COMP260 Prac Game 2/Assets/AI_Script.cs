﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AI_Script : MonoBehaviour
{
    Rigidbody rb;

    public float speed = 5.0f;
    public float force = 10f;
    float x;
    float y;

    // Use this for initialization
    void Start()
    {
        GetComponent<Rigidbody>().useGravity = false;
        rb = GetComponent<Rigidbody>();
        y = 0.5f;
    }


    void FixedUpdate()
    {
        rb.velocity = new Vector3(speed * x, 0, speed * y);

    }

    private Vector3 GetMousePosition()
    {
        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }
    void OnCollisionEnter (Collision collider)
    {
            y = -y;
    }
}

