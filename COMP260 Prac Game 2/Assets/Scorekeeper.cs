﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Scorekeeper : MonoBehaviour {

    static private Scorekeeper instance;

    public Text[] winner;
    public int pointsPerGoal = 1;
    private int[] score = new int[2];

public Text[] scoreText;
public void OnScoreGoal(int player)
    {
        score[player] += pointsPerGoal;
        scoreText[player].text = score[player].ToString();        if (score[player] == 2)
        {
            winner[0].text = ("Player " + player + " wins").ToString();
            Debug.Log("Player " + player + " wins!");
            for (int i = 0; i < score.Length; i++)
            {
                score[i] = 0;
            }
            scoreText[0].text = score[0].ToString();
            scoreText[1].text = score[1].ToString();

            
            Time.timeScale = 0.0f;
        }        
    }


    static public Scorekeeper Instance
    {
        get { return instance; }
    }
    void Start()
    {
        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError(
            "More than one Scorekeeper exists in the scene.");
        }

        // reset the scores to zero
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
}
